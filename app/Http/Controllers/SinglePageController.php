<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Class SinglePageController
 * @package App\Http\Controllers
 */
class SinglePageController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view("home");
    }
}
