<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CitiesController extends Controller
{
    public function view($city){
        $city  = City::with(['weathers'])->where(['name' => $city])->firstOrFail();

        return response()->json([
            'response' => 'success',
            'city' => $city
        ], Response::HTTP_OK);
    }
}
