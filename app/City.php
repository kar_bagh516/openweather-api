<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $fillable = [
        'name',
        'lat',
        'lng',
        'created_at',
        'updated_at'
    ];

    public function weathers(){
        return $this->hasMany(Weather::class);
    }
}
