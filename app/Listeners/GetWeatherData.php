<?php

namespace App\Listeners;

use App\City;
use App\Events\NewWeatherCall;
use App\Weather;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class GetWeatherData
{
    private const URI = 'http://api.openweathermap.org/data/2.5/weather';


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param NewWeatherCall $event
     */
    public function handle(NewWeatherCall $event)
    {

        $cities = City::all();
        $client = new Client();
        $responses = [];

        foreach ($cities as $city) {
            $response = json_decode($client->get(self::URI, [
                'query' => [
                    'lat' => $city->lat,
                    'lon' => $city->lng,
                    'appid' => config('app.weather_key'),
                    'units' => 'metric',
                ]
            ])->getBody()->getContents());


            $responses []= [
                'city_id' => $city->id,
                'time' => Carbon::createFromTimestamp($response->dt)->format('Y-m-d H:i:s'),
                'temp' => $response->main->temp,
                'pressure' => $response->main->pressure,
                'humidity' => $response->main->humidity,
                'temp_min' => $response->main->temp_min,
                'temp_max' => $response->main->temp_max,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }

        Weather::insert($responses);
    }
}
