<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    protected $table = 'weathers';

    protected $fillable = [
        'weather',
        'city_id',
        'time',
        'temp',
        'pressure',
        'humidity',
        'temp_min',
        'temp_max',
        'created_at',
        'updated_at'
    ];
}
