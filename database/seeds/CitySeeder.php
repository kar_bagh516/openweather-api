<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                'name' => 'Yerevan',
                'lat' => 40.18,
                'lng' => 44.51,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],[
                'name' => 'Kapan',
                'lat' => 39.21,
                'lng' => 46.41,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        DB::table('cities')->insert($cities);
    }
}
